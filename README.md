# TODO_tasks

1) Реализовать конструкторы классов: <br/>
    1.1) Connection <br/>
    1.2) ConnectionManager <br/>
    1.3) CommandType <br/>
    1.4) Command <br/>
2) Реализовать функции: <br/>
    2.1) ConnectionManager::sendComand(cmd) <br/>
    2.2) ... <br/>
3) Реализовать сериализацию <br/>
4) Разобраться как работать с : <br/>
    4.1) QTcpSocket <br/>
    4.2) std::bind, std::function <br/>
    4.3) инициализацией умных указателей не в момент создания <br/>
    **4.4) QSettings (done)** <br/>
<br/> <h3> P.s. Список не полный, это что в голову пришло сходу <h3/>

