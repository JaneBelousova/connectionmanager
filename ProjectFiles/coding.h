#ifndef CODING_H
#define CODING_H
#include <QDataStream>

const char startByte = 0x81;
const char stopByte = 0x82;
const char escByte = 0x83;

QByteArray encoding(QByteArray);
QByteArray decoding(QByteArray);

#endif // CODING_H
