#include "command.h"

#include <QDataStream>

Command::Command() {}

Command::Command(const CommandType& typeSet, const QVariant& dataSet) {
    setType(typeSet);
    setData(dataSet);
}

void Command::setType(const CommandType& typeSet)
{
    type = typeSet;
}

void Command::setData(const QVariant& dataSet)
{
    data = dataSet;
}

CommandType Command::getType() const
{
    return type;
}

QVariant Command::getData() const
{
    return data;
}

QDataStream& operator>>(QDataStream& str, Command& cmd)
{
    CommandType typeSet;
    QVariant dataSet;
    str >> typeSet >> dataSet;
    cmd.setType(typeSet);
    cmd.setData(dataSet);
    return str;
}

QDataStream& operator<<(QDataStream& str, const Command& cmd)
{
    str << cmd.getType() << cmd.getData();
    return str;
}
