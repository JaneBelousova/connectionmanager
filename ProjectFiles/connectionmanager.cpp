#include <connectionmanager.h>

ConnectionManager::ConnectionManager()
{
    //настройка пользовательских типов
    int id; //Metatype id

    //использовать тип T в подключенных к очереди сигнальных и слот-соединениях
    id = qRegisterMetaType<CommandType>();
    qDebug() << "CommandType: metatype id = "<< id;
    id = qRegisterMetaType<Command>();
    qDebug() << "Command: metatype id = "<< id;

    // Регистрирует операторы потока для типа. Используются при потоковой передаче QVariant
    qRegisterMetaTypeStreamOperators<CommandType>("Command");
    qRegisterMetaTypeStreamOperators<Command>("Command");

    connect(&server, &QTcpServer::newConnection, this, &ConnectionManager::addConnection);

    std::unique_ptr<QSettings> tempUniqueSettings(new QSettings("Prosoft_school2019", "ConnectionManager"));
    settings.swap(tempUniqueSettings);

    numberOfNodes = settings->value("numberOfNodes").toInt();
    nodeNumber = 0; // (?) settings->value()

    QString address;
    QString port;

    for (int i = nodeNumber + 1; i < numberOfNodes; ++i) {
        //QString connectionStr = "Node" + QString::number(i);
        QString nodeStr = "Node" + QString::number(i);
        std::unique_ptr<QSettings> settings_i = std::move(settings);
        settings_i->beginGroup("Nodes");
            settings_i->beginGroup(nodeStr);
                address = settings_i->value("address").toString();
                port = settings_i->value("port").toString();

                Connection connection(address, port);

            settings_i->endGroup();
        settings_i->endGroup();

        for (auto j : connection.supportedCommands) {
            typedCons.insert(j,connection);
        }

        cons.push_back(connection);
    }
}

void ConnectionManager::addConnection()
{
    Connection connection(server.nextPendingConnection());
    cons.push_back(connection);
}
