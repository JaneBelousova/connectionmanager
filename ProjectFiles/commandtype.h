#ifndef COMMANDTYPE_H
#define COMMANDTYPE_H
#include <QCoreApplication>

class CommandType {
    enum Operator
    {
    INSERT,
    UPDATE,
    SELECT,
    DELETE,
    EXECUTE
    };
public:
    CommandType();
    CommandType(const CommandType::Operator _operatorSet,
                const QString& _operandSet);
    void set_operator(const CommandType::Operator _operatorSet);
    void set_operand(const QString& _operandSet);
    CommandType::Operator get_operator() const;
    QString get_operand() const;
private:
    Operator _operator;
    QString _operand;

    friend QDataStream& operator>>(QDataStream& str, CommandType& type);
    friend QDataStream& operator<<(QDataStream& str, const CommandType& type);
};
// регистрируем тип в QVariant
Q_DECLARE_METATYPE(CommandType);

#endif // COMMANDTYPE_H
