#include "coding.h"

QByteArray encoding(QByteArray arr) {
    arr.insert(0,startByte);
    int i = 1;
    while (i < arr.size()) {
        if (arr[i] == startByte || arr[i] == stopByte || arr[i] == escByte) {
            arr.insert(i,escByte);
            ++i;
        }
        ++i;
    }
    arr.insert(i,stopByte);
    return arr;
}
QByteArray decoding(QByteArray arr) {
    arr.remove(0,1);
    int i = 0;
    while (1) {

        if (arr[i] == stopByte) {
            arr.remove(i,1);
            break;
        }
        if (arr[i] == escByte)
            arr.remove(i,1);
        ++i;
    }
    return arr;
}
