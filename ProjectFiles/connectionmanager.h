#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H
#include <QCoreApplication>
#include <QObject>
#include <QSettings>
#include <connection.h>
#include <command.h>
#include <commandtype.h>
#include <QTcpServer>

class ConnectionManager: public QObject {
    Q_OBJECT
public:
    /*Список соединений в сети.
    * Каждое присоединение (соответственно классу Connection) это
    * сокет и список команд, поддерживаемых данным соединением
    */
    QVector<Connection> cons;
    void sendCommand(const Command& cmd);
    ConnectionManager();
    void addConnection();

signals:
    void incomingCommand(Command cmd);
private:
    //Пары: команда || все присоединения, способные данную команду обрабатывать

    std::unique_ptr<QSettings> settings;
    QMultiMap<CommandType,Connection> typedCons;
    int nodeNumber;
    int numberOfNodes;
    QTcpServer server;

};

#endif // CONNECTIONMANAGER_H
