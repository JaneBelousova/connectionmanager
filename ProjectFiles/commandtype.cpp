#include"commandtype.h"

#include <QDataStream>

CommandType::CommandType() {}

CommandType::CommandType(const CommandType::Operator _operatorSet,
                         const QString& _operandSet) {
    set_operator(_operatorSet);
    set_operand(_operandSet);
}

void CommandType::set_operator(const CommandType::Operator _operatorSet)
{
    _operator = _operatorSet;
}

void CommandType::set_operand(const QString& _operandSet)
{
    _operand = _operandSet;
}

CommandType::Operator CommandType::get_operator() const
{
    return _operator;
}

QString CommandType::get_operand() const
{
    return _operand;
}

QDataStream& operator>>(QDataStream& str, CommandType& type)
{
    qint8 _operatorSet;
    QString _operandSet;
    str >> _operatorSet >> _operandSet;
    type.set_operator(static_cast<CommandType::Operator>(_operatorSet));
    type.set_operand(_operandSet);
    return str;
}

QDataStream& operator<<(QDataStream& str, const CommandType& type)
{
    str << static_cast<qint8>(type.get_operator()) << type.get_operand();
    return str;
}
