#ifndef COMMAND_H
#define COMMAND_H
#include <QCoreApplication>
#include <commandtype.h>
#include <QVariant>

//представляет собой типизированное сообщение (или команду),
//используется для обмена между плагином и менеджером соединений.
//Содержит пару: тип команды и данные (объект действия команды).
//объявлены дружественные функции (де)сериализации (из)в поток(а).
class Command {
public:
    Command();
    Command(const CommandType& typeSet, const QVariant& dataSet);
    void setType(const CommandType& typeSet);
    void setData(const QVariant& dataSet);
    CommandType getType() const;
    QVariant getData() const;
private:
    CommandType type;
    QVariant data;
    friend QDataStream& operator>>(QDataStream& str, Command& cmd);
    friend QDataStream& operator<<(QDataStream& str, const Command& cmd);
};
// регистрируем тип в QVariant
Q_DECLARE_METATYPE(Command);

#endif // COMMAND_H
