#include <connection.h>
#include <coding.h>

Connection::Connection(QTcpSocket* newSocket, QObject *parent):
                        QObject(parent),
                        socketReader(newSocket),
                        socket(newSocket)
{
    communication();
    sendSupportedCommands();
}

Connection::Connection(QString address,
                       QString port,
                       QObject *parent):QObject(parent)
{
    socket = new QTcpSocket(this);
    socketReader.setDevice(socket);

    communication();
    startConnect(address, port);
}

void Connection::communication()
{
    connect(socket, &QTcpSocket::connected, this, &Connection::sendSupportedCommands);
    connect(socket, &QTcpSocket::disconnected, this, &Connection::startConnect);
    connect(socket, &QTcpSocket::readyRead, this, &Connection::readData);
    connect(this, &Connection::incCommand, this, &Connection::sendCommand);
}

void Connection::startConnect(QString address, QString port)
{
    QVariant v_port;
    v_port.setValue(port);
    socket->connectToHost(address, v_port.value<quint16>()); // read and write
}

void Connection::sendSupportedCommands()
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream << supportedCommands;

    socket->write(encoding(data));
}

void Connection::sendCommand(const Command& cmd)
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream << cmd;

    socket->write(encoding(data));
}

int Connection::readData(QDataStream& stream)
{
    QByteArray coding_data;
    bool commitTransaction = true;
    while (commitTransaction
                    && stream.device()->bytesAvailable() > 0)
    {
        stream.startTransaction();
        stream >> coding_data;
        commitTransaction = stream.commitTransaction();
    }

    if(coding_data[0]!=startByte && coding_data[coding_data.size()-1]!=stopByte)
    {
        qDebug() << "Data transfer error";
        emit error(socket->error());
        return EXIT_FAILURE;
    }

    QByteArray data = decoding(coding_data);

    Command cmd;
    CommandType Type;
    QVariant v;
    v.setValue(data);
    if (v.canConvert<CommandType>())
    {
        Type = v.value<CommandType>();
        supportedCommands.push_back(Type);
    }
    else if(v.canConvert<Command>())
    {
        cmd = v.value<Command>();
        emit incCommand(cmd);
    }
    else
    {
        qDebug() << "Type of incoming data is not defined";
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
