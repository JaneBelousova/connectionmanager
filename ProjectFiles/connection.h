#ifndef CONNECTION_H
#define CONNECTION_H
#include <QCoreApplication>
#include <commandtype.h>
#include <command.h>
#include <memory>
#include <QTcpSocket>
#include <QObject>
#include <QSettings>
#include <QDataStream>



class Connection : public QObject {
    Q_OBJECT
public:
    Connection(QTcpSocket* newSocket, QObject *parent=0);
    Connection(QString adress,
               QString port,
               QObject *parent = 0);
    //список команд, поддерживаемых данным соединением
    QVector<CommandType> supportedCommands;
    void communication();
    void startConnect(QString address, QString port);
    void sendSupportedCommands();
    void sendCommand(const Command& cmd);
    int readData(QDataStream& stream);
signals:
    void incCommand(Command cmd);
    void error(QTcpSocket::SocketError socketError);

private:
    QDataStream socketReader;
    QTcpSocket* socket;
};

#endif // CONNECTION_H
